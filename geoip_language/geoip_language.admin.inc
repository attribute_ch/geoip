<?php

/**
 * @file
 * Admin page callbacks for the GeoIP Language module.
 */

/**
 * Menu callback for admin/config/regional/language/configure/geoip.
 */
function geoip_language_settings_overview() {
  if (!language_negotiation_get_any(GEOIP_LANGUAGE_NEGOTIATION_PATH)) {
    drupal_set_message(t('The GeoIP settings will have no effect until <em>Language Negotiation</em> is set to <em>Path prefix with GeoIP detection fallback</em> on the <a href="@language-configure">Language configuration page</a>.', array('@language-configure' => url('admin/config/regional/language/configure'))), 'warning');
  }
  $output = '';
  $countries = geoip_country_values();
  $languages = locale_language_list('name', TRUE);
  $mapping = geoip_language_mappings();

  $rows = array();
  foreach ($mapping as $country => $language) {
    $rows[] = array(
      $countries[$country] . ' (' . $country . ')',
      $languages[$language] . ' (' . $language . ')',
      l(t('Delete'), 'admin/config/regional/language/configure/geoip/delete/' . $country, array('query' => drupal_get_destination())),
    );
  }
  if (count($rows)) {
    $header = array(
      array('data' => t('Country')),
      array('data' => t('Language')),
      t('Operations'),
    );
    $output .= theme('table', array('header' => $header, 'rows' => $rows));
  }
  else {
    drupal_set_message(t('No GeoIP language mappings defined.'));
  }

  $form = drupal_get_form('geoip_language_settings_form');
  $output .= drupal_render($form);
  return $output;
}

/**
 * FAPI callback for creating a new country-language mapping.
 */
function geoip_language_settings_form() {
  $countries = geoip_country_values();
  $mapping = geoip_language_mappings();
  $options = array();
  foreach ($countries as $key => $value) {
    if (!isset($mapping[$key])) {
      $options[$key] = "$key - $value";
    }
  }

  $form['new'] = array(
    '#type' => 'fieldset',
    '#title' => t('New mapping'),
    '#tree' => 0,
  );
  $form['new']['country'] = array(
    '#type' => 'select',
    '#title' => t('Detected country'),
    '#options' => $options,
  );
  $form['new']['language'] = array(
    '#type' => 'select',
    '#title' => t('Language'),
    '#options' => locale_language_list('name', TRUE),
  );

  $form['new']['submit'] = array('#type' => 'submit', '#value' => t('Add mapping'));

  return $form;
}

/**
 * FAPI submit handler.
 */
function geoip_language_settings_form_submit($form, &$form_state) {
  geoip_language_mapping_create($form_state['values']['country'], $form_state['values']['language']);

  $countries = geoip_country_values();
  drupal_set_message(t('GeoIP mapping created for %country.', array('%country' => $countries[$form_state['values']['country']])));

  $form_state['redirect'] = 'admin/config/regional/language/configure/geoip';
}

/**
 * Delete confirmation form.
 */
function geoip_admin_delete_mapping($form, &$form_state, $country) {
  $form['country'] = array(
    '#type' => 'value',
    '#value' => $country,
  );

  return confirm_form($form,
    t('Are you sure you want to delete this mapping?'),
    isset($_GET['destination']) ? $_GET['destination'] : 'admin/config/regional/language/configure/geoip',
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Delete mapping.
 */
function geoip_admin_delete_mapping_submit($form, &$form_state) {
  geoip_language_mapping_delete($form_state['values']['country']);

  $countries = geoip_country_values();
  drupal_set_message(t('GeoIP mapping deleted for %country.', array('%country' => $countries[$form_state['values']['country']])));

  $form_state['redirect'] = 'admin/config/regional/language/configure/geoip';
}
